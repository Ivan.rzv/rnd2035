import { configureStore } from '@reduxjs/toolkit'

import rootReducer from './newsSlice'

const store = configureStore({ reducer: rootReducer })

export default store