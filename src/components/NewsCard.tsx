import React from 'react';
import { Card } from "antd";
import { Link } from "react-router-dom";
function NewsCard(props: any){
    const date = new Date(props.item.publishedAt)
    return (
        <Card 
        style={{marginTop: '24px'}} 
        title={props.item.title} 
        extra={ <div>{
            date.toLocaleDateString()}</div>} 
        >
            <p>{props.item.description} <Link to={props.item.title}> Read more...</Link></p>
        </Card>
    )
}

export default NewsCard