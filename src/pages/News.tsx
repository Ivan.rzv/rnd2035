import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { NewsState, fillNews } from '../newsSlice'
import NewsCard from '../components/NewsCard';
import { PageHeader, Button, Layout, Typography } from 'antd';

const { Content } = Layout;
const { Title } = Typography

const fetchNews = () =>{
  //@ts-ignore
  return async dispatch => {
    const data = await fetch('https://newsapi.org/v2/everything?q=education&apiKey=e87f2f035b01433daf9ce16df79a4cca').then((data)=> data.json())
    dispatch(fillNews(data.articles))
  }
}


function News() {
    const allNews = useSelector((state: any) => state.news)
    const dispatch = useDispatch()
  return (
    <div className="News">
      <Layout>
        <PageHeader className="News-header" title="News page" extra={
          <Button 
          type="primary" 
          onClick={
            //@ts-ignore
            () => dispatch(fetchNews())
          }
          >Reload</Button>
        } />
        <Content style={{ margin: '24px ' }}>
          { allNews.map((item: NewsState)=>
            <NewsCard key={item.title} item={item} />
          )}

          {allNews.length === 0 && <Title> Reload news! </Title>}
        
        </Content>
      </Layout>
    </div>
  );
}

export default News;