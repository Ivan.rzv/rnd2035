import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from "react-router-dom";
import { decrement, NewsState } from '../newsSlice'
import { PageHeader, Button, Layout } from 'antd';

const { Content } = Layout;


function NewsItem() {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const allNews = useSelector((state: any) => state.news)
  const thisArticle = allNews.filter((item: NewsState)=>params.title!.replace(/[^a-zA-Z ]/g, "") === item.title.replace(/[^a-zA-Z ]/g, ""))[0]

  const date = new Date(thisArticle.publishedAt)

  return (
    <div className="NewsItem">
      <Layout>
        <PageHeader 
        onBack={() => navigate('/news')} 
        title={params.title} subTitle={date.toLocaleDateString()} 
        className="NewsItem-header"
        extra={<Button type="primary" onClick={()=> {
          dispatch(decrement(params.title))
          navigate('/news')
          }} danger>delete</Button>}
        >
          
        </PageHeader>
        <Content style={{ margin: '24px ' }}>
            <p>{ thisArticle.content }</p>
        </Content>
      </Layout>
    </div>
  );
}

export default NewsItem;