import { createSlice } from '@reduxjs/toolkit'

export interface NewsState {
  title: string;
  author: string;
  description: string;
  url: string;
  publishedAt: string;
  content: string;
}

const initialState = {news:[] as NewsState[]}

const newsSlice = createSlice({
  name: 'News',
  initialState,
  reducers: {
    decrement(state, action) {
      state.news = state.news.filter((item)=>item.title.replace(/[^a-zA-Z ]/g, "") !== action.payload.replace(/[^a-zA-Z ]/g, ""))
    },
    fillNews(state, action) {
      state.news = action.payload
    }
  },
})

export const { decrement, fillNews } = newsSlice.actions
export default newsSlice.reducer